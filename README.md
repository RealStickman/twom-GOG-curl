# This War of Mine GOG libcurl.so.4 fix

The libcurl.so.4 package in arch-based distros does not provide backwards compatibility with CURL_OPENSSL_3, which is needed for the game "This War of Mine" to run through lutris.

The start.sh file will be replacing the original start.sh file in the game folder to load the proper version of libcurl. Currently this has to be done manually.

The setup.sh file is used to install the package needed for earlier libcurl support. In the future it will also install the start.sh file into the proper folder to make that process automatic.