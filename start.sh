#!/bin/bash
# GOG.com (www.gog.com)
# This War Of Mine
# Altered for use with Arch-based distributions

# select curl version
LD_PRELOAD=libcurl.so.3

# Initialization
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${CURRENT_DIR}"
source support/gog_com.shlib

# Game info
GAME_NAME="$(get_gameinfo 1)"
VERSION="$(get_gameinfo 2)"
VERSION_DEV="$(get_gameinfo 3)"

# Actions
run_game() {
 echo "Running ${GAME_NAME}"
 cd game
 chmod +x "This War of Mine"
 ./"This War of Mine"
}

default() {
  run_game
}

# Options
define_option "-s" "--start" "start ${GAME_NAME}" "run_game" "$@"

# Defaults
standard_options "$@"
